var url = 'http://localhost:9081/todos';
axios.get(url).then(function(res) {
    var items = res.data;
    render(items);
});

function render(items) {
    var todoListElement = document.getElementById('todo-list');
    var content = items.map(function(item) {
        return '<li>' + item.content + '</li>';
    });
    todoListElement.innerHTML = content.join('');
}