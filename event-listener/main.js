var todoList = [
    'Take a shower',
    'Have a walk',
    'Go to bed'
];

function render() {
    var todoListElement = document.getElementById('todo-list');
    var content = todoList.map(function(item) {
        return '<li>' + item + '</li>';
    });
    todoListElement.innerHTML = content.join('');
}

function addItem() {
    var input = document.getElementById('new-item');
    var value = input.value;
    todoList.push(value);
    render();
    input.value = '';
}

var addBtn = document.getElementById('add-btn');
addBtn.addEventListener('click', addItem);
addBtn.addEventListener('click', function() {
    console.log('Button is clicked!!!');
});

render();