var key = 'todo-list';
var todoListStr = localStorage.getItem(key);
var todoList = todoListStr === null ? [] : JSON.parse(todoListStr);

var addBtn = document.getElementById('add-btn');
addBtn.addEventListener('click', addItem);

function render() {
    var todoListElement = document.getElementById('todo-list');
    var content = todoList.map(function(item) {
        return '<li>' + item + '</li>';
    });
    todoListElement.innerHTML = content.join('');
}

function addItem() {
    var inputElement = document.getElementById('new-item');
    var value = inputElement.value;
    todoList.push(value);
    render();
    inputElement.value = '';
    localStorage.setItem(key, JSON.stringify(todoList));
}

render();