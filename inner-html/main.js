var todoList = [
    'Take a shower',
    'Have a walk',
    'Go to bed'
];
var todoListElement = document.getElementById('todo-list');
var content = todoList.map(function(item) {
    return '<li>' + item + '</li>';
});
todoListElement.innerHTML = content.join('');