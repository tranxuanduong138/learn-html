var todoList = [
    'Take a shower',
    'Have a walk',
    'Go to bed'
];

function render() {
    var todoListElemet = document.getElementById('todo-list');
    var content = todoList.map(function(item) {
        return '<li>' + item + '</li>';
    });
    todoListElemet.innerHTML = content.join('');
}

function addItem() {
    // get value of the input
    var input = document.getElementById('new-item');
    var value = input.value;
    // add to todoList array
    todoList.push(value);
    // re-render
    render();
    // clear input
    input.value = '';
}

render();