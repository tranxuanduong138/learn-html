/**
 * HTML Element
 * 
 * - Cây thừa kế: EvenTarget <-- Node <-- Element <-- HTMLElement
 * - Properties:
 *  + .children (Element): trả về danh sách element con của 1 element  
 *  + .textContent (Node): trả về nội dung của 1 element
 * - Methods:
 * .appendChild (Node): thêm 1 element vào danh sách element con của 1 element
 * .remove (Element): xóa 1 element
 */

var todoList = document.getElementById('todo-list');
console.log('Before insert', todoList);
// Insert a new todo to the list
var newTodo = document.createElement('li');
newTodo.textContent = 'Cook a breakfast';
console.log(newTodo);
todoList.appendChild(newTodo);
console.log('After insert', todoList);
// Remove the last item fromt the list
todoList.lastElementChild.remove();
console.log('After remove', todoList);

// ---> Kết quả không như mong đợi ? Bất động bộ