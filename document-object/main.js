/**
 * document object
 * 1. Properties
 * document.head
 * document.body
 * 2. Methods
 * document.getElementById()
 */

console.log(document.head);
console.log(document.body);
console.log(document.getElementById('entry1'));