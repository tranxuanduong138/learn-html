var students = [
    {name: 'A', age: 18},
    {name: 'B', age: 18},
    {name: 'C', age: 19},
    {name: 'D', age: 19},
    {name: 'E', age: 20}
];

var selectedAgeElement = document.getElementById('selected-age');
selectedAgeElement.addEventListener('change', filerAge);

function render(students) {
    var studentsElement = document.getElementById('student-table');
    var content = students.map(function(student) {
        return '<tr><td>' + student.name + '</td><td>' + student.age + '</td></tr>';
    });
    studentsElement.innerHTML = content.join('');
}

function filerAge() {
    var selectedAge = parseInt(selectedAgeElement.value);
    var filtedStudents = students.filter(function(student) {
        return student.age === selectedAge;
    });
    render(filtedStudents);
}

render(students);